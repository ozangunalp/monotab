var monotab = false;
var triedToOpen = [];
// register toggle pin
var pinCommand = function(command) {
  if (command == "toggle-pin") {
	  console.info("pin!")
	 // Get the currently selected tab
	 chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		// Toggle the pinned status
		var current = tabs[0]
		chrome.tabs.update(current.id, {'pinned': !current.pinned});
	 });
  }
};

// register button click listener
var buttonClicked = function(tab) {
	monotab = !monotab;	
	triedToOpen = [];
	var details = {};
	if(monotab){
		details.path = {'38': "images/m_logo128px_large_red.png"}
		chrome.tabs.query({pinned: false, currentWindow: true}, function(tabs){
			for(var i = 0; i< tabs.length; ++i){
				var tab = tabs[i]
				monoizeLinks(tab);
				chrome.webRequest.onCompleted.addListener(
					function(event){ // callback
						if(event.tabId>0){
							chrome.tabs.get(event.tabId, function(tab){
								if(tab !== undefined){
									monoizeLinks(tab)
								}
							})
						}
					}, // filter
					{
						urls: ["http://*/*","https://*/*"],
						'tabId':tab.tabId
					})
			}
		});		
	} else {
		details.path = {'38': "images/m_logo128px_large_black.png"};
		chrome.browserAction.setBadgeText({'text':""})
		var savedTabs = chrome.extension.getURL("savedTabsPopup.html")
		chrome.tabs.create({url:savedTabs, active:true})
		chrome.runtime.sendMessage({greeting: "hello"}, function(response) {
		  	console.log(response.farewell);
		});
	}
	chrome.browserAction.setIcon(details);
};

// fast onCreate listener callback	
var fast = function(tab) {
	var time = new Date();
	var tabLength = 1;
	if(monotab && !tab.pinned && !chromeTab(tab)){
		// 	count the number of tabs
		chrome.tabs.query({pinned: false, currentWindow: true}, function(tabs){
			tabLength = tabs.length;
			if(tabLength!=1){
				triedToOpen.push({url:tab.url})
				console.info(new Date()-time)
				console.info(triedToOpen)				
				chrome.tabs.update(current.id, {'highlighted': true});
			}				
		});	
	}
};

// slow onCreate listener callback
var slow = function(tab) {
	var time = new Date();
	var tabLength = 1
	chrome.tabs.query({pinned: false, currentWindow: true}, function(tabs){
		tabLength = tabs.length
		if(monotab && !tab.pinned && !chromeTab(tab) && tabLength!=1){
			// TODO tab.openerTabId is sometimes undefined
			var currentURL = null;
			if(tab.openerTabId !== undefined){
				chrome.tabs.get(tab.openerTabId, function(current){
					if(current !== undefined){
						currentURL = current.url
					}
					if(!current.pinned){
						addToList({to:tab.url, from:currentURL})
						chrome.tabs.remove(tab.id)
					}
				})
			} else {
				addToList({to:tab.url, from:currentURL})
				chrome.tabs.remove(tab.id)
			}			
		}
	})
}

function chromeTab(tab){
	
	if(tab.url.lastIndexOf("chrome://newtab/", 0)===0){
		return false;
	}
	if(tab.url.lastIndexOf("chrome://", 0)===0){
		return true;
	}
	return false;
}

function executeContentScript(tab){
	if(!chromeTab(tab)){
		chrome.tabs.executeScript(tab.id, {file:"content.js"})	
	}	
}

function addToList(open){
	triedToOpen.push(open)
	chrome.browserAction.setBadgeText({'text':""+triedToOpen.length});
	console.info(open)
}

function monoizeLinks(tab){
	if(monotab){
		executeContentScript(tab)
	}
}

chrome.commands.onCommand.addListener(pinCommand);
chrome.browserAction.onClicked.addListener(buttonClicked);
chrome.tabs.onCreated.addListener(slow);

chrome.tabs.onUpdated.addListener(function(tabId,changeInfo,tab){
	monoizeLinks(tab)
});

